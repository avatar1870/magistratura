﻿using System;
using System.Windows.Forms.DataVisualization.Charting;

public class Vizualizer
{
    private Chart _chart;

    public Vizualizer(Chart chart)
	{
        _chart = chart;
	}

    public void Draw2D()
    {
        _chart.ChartAreas.Add(new ChartArea("Math functions"));
    }
}
