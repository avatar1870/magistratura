﻿using System;
using System.Collections.Generic;

using System.Drawing;
using System.Windows.Forms;

using MIConvexHull;
using System.Linq;
using System.IO;

namespace StructuredLight
{
    public partial class Form1 : Form
    {
        private double f;
        private double alpha;
        private double b;

        private List<Vector2Int> points = new List<Vector2Int>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double.TryParse(textBox1.Text, out f);
            double.TryParse(textBox2.Text, out alpha);
            double.TryParse(textBox3.Text, out b);

            Calculate calculate = new Calculate(b, f, alpha);

            foreach (Vector2Int vector2Int in points)
            {
                var newPoint = calculate.GetPoint(vector2Int.x, vector2Int.y);

                richTextBox1.Text += newPoint.X + " " + newPoint.Y + " " + newPoint.Z + Environment.NewLine;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "Image file|*.jpg;*.png;*.bmp" };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Bitmap bitmapImage = Bitmap.FromFile(openFileDialog.FileName) as Bitmap;

                int width = bitmapImage.Width - 1;
                int height = bitmapImage.Height - 1;

                Color pixelColor;

                points.Clear();

                for (int x = 0; x < width; ++x)
                {
                    for (int y = 0; y < height; ++y)
                    {
                        pixelColor = bitmapImage.GetPixel(x, y);

                        if (Color.FromArgb(0, pixelColor) == Color.FromArgb(0, Color.Black))
                        {
                            points.Add(new Vector2Int(x, y));
                        }
                    }
                }

                MessageBox.Show("Конец вычислений");

                Form2 form = new Form2();
                form.ShowShow(points);
                form.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<Point3D> _pointList = new List<Point3D>()
            {
                new Point3D(0.0M, 0.0M, 0.0M),
                new Point3D(1M, 0.0M, 0.0M),
                new Point3D(0.0M, 0.0M, 1M),
                new Point3D(1M, 0.0M, 1M),

                new Point3D(1M, 1M, 0.0M),
                new Point3D(0.0M, 1M, 1M),
                new Point3D(0.0M, 1M, 0.0M),
                new Point3D(1M, 1M, 1M),
            };

            //List<Triangle> f = new List<Triangle>();
            //List<Facet> facets = new List<Facet>();

            //_pointList.Clear();
            //Enumerable.Range(0, 20).ForEach(item => _pointList.Add(new Vector3D(Point3D.GetRandom(5))));

            Delaunay delaunay = new Delaunay(_pointList);

            /*MessageBox.Show(delaunay.AllTriangleList.Count.ToString());
            MessageBox.Show(delaunay.TetraList.Count.ToString());
            MessageBox.Show(delaunay.OutsideTriangleList.Count().ToString());*/

            List<Facet> facets = new List<Facet>();
            foreach (Triangle tr in delaunay.OutsideTriangleList)
            {
                facets.Add(tr.GetFacet());
            }


            new STLDocument("Test", facets).SaveAsText(@"C:/ABS/m.stl");

            //Draw model

            new Form3(delaunay).Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<Bitmap> bitmaps = new List<Bitmap>();

            OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "Image file | *.jpg;*.png;*.bmp" };
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (string fileName in openFileDialog.FileNames)
                {
                    bitmaps.Add(Bitmap.FromFile(fileName) as Bitmap);
                }

                Calculate c = new Calculate(0f, 0f, 0f);
                c.CalculateLineNumber(bitmaps.ToArray());

                //test

                pictureBox1.Image = c._citmap;

                //test = 
            }
        }
    }
}
