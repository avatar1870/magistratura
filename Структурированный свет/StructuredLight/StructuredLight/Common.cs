﻿using System;

namespace StructuredLight
{
    class Common
    {
        public static double ToRoundDown(double dValue, int iDigits)
        {
            double dCoef = Math.Pow(10, iDigits);

            return dValue > 0 ? Math.Floor(dValue * dCoef) / dCoef :
                                Math.Ceiling(dValue * dCoef) / dCoef;
        }

        public static decimal GetDist(Vector3D vector1, Vector3D vector2)
        {
            decimal width = vector1.Vertex.X - vector2.Vertex.X;
            decimal height = vector1.Vertex.Y - vector2.Vertex.Y;
            decimal depth = vector1.Vertex.Z - vector2.Vertex.Z;

            decimal dist = (decimal)(Math.Sqrt(Math.Pow((double)width, 2) +
                                                Math.Pow((double)height, 2) +
                                                Math.Pow((double)depth, 2)));

            return dist;
        }
    }
}
