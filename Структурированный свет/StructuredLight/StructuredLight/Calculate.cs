﻿using System.Numerics;
using System;
using System.Windows.Forms;
using System.Drawing;
using StructuredLight.Data;

namespace StructuredLight
{
    class Calculate
    {
        private double b;
        private double f;
        private double alpha;

        public Calculate(double b, double f, double alpha)
        {
            this.b = b;
            this.f = f;
            this.alpha = alpha;
        }

        public Point3D GetPoint(int x, int y)
        {
            Point3D point = new Point3D
            (
                (float)(Math.Tan(alpha) * b * x) / (f + x * Math.Tan(alpha)),
                (float)(Math.Tan(alpha) * b * y) / (f + x * Math.Tan(alpha)),
                (float)(Math.Tan(alpha) * b * f) / (f + x * Math.Tan(alpha))
            );

            return point;
        }

        public void CalculateLineNumber(Bitmap[] bitmaps)
        {
            if (bitmaps != null)
            {
                ImageRepresentation finalImage = new ImageRepresentation();

                Color pixelColor;

                foreach (Bitmap bitmapImage in bitmaps)
                {
                    int width = bitmapImage.Width - 1;
                    int height = bitmapImage.Height - 1;

                    for (int x = 0; x < width; ++x)
                    {
                        for (int y = 0; y < height; ++y)
                        {
                            pixelColor = bitmapImage.GetPixel(x, y);

                            //TODO исправить определение цвета
                            finalImage.AddOrUpdatePoint(x, y,
                                (Color.FromArgb(0, pixelColor) == Color.FromArgb(0, Color.Black) ? "1" : "0"));
                        }
                    }
                }

                MessageBox.Show("Finish");
            }            
        }
    }
}
