﻿using System;

namespace StructuredLight
{
    public class Vector3D : IEquatable<Vector3D>
    {
        public Point3D Vertex { get; private set; }

        public Vector3D(Point3D vertex)
        {
            Vertex = vertex;
        }

        public Vector3D Cross(Vector3D sourceVector3D)
        {
            decimal crossX = Vertex.Y * sourceVector3D.Vertex.Z - Vertex.Z * sourceVector3D.Vertex.Y;
            decimal crossY = Vertex.Z * sourceVector3D.Vertex.X - Vertex.X * sourceVector3D.Vertex.Z;
            decimal crossZ = Vertex.X * sourceVector3D.Vertex.Y - Vertex.Y * sourceVector3D.Vertex.X;

            return new Vector3D(new Point3D(crossX, crossY, crossZ));
        }

        public decimal Dot(Vector3D sourceVector3D)
        {
            return Vertex.X * sourceVector3D.Vertex.X +
                Vertex.Y * sourceVector3D.Vertex.Y +
                Vertex.Z * sourceVector3D.Vertex.Z;
        }

        public Vector3D Sub(Vector3D sourceVector3D)
        {
            Vector3D distance = new Vector3D(new Point3D(Vertex.X - sourceVector3D.Vertex.X,
                Vertex.Y - sourceVector3D.Vertex.Y,
                Vertex.Z - sourceVector3D.Vertex.Z));
            return distance;
        }

        public Vector3D GetNormalization()
        {
            decimal length = (decimal)Math.Sqrt((double)((Vertex.X * Vertex.X) 
                + (Vertex.Y * Vertex.Y) 
                + (Vertex.Z * Vertex.Z)));

            Vector3D dst = new Vector3D(new Point3D(this.Vertex.X / length,
                                      this.Vertex.Y / length,
                                      this.Vertex.Z / length));

            return dst;
        }

        public bool Equals(Vector3D other)
        {
            if (other != null)
            {
                return other.Vertex.Equals(Vertex);
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}", Vertex.ToString());
        }
    }
}
