﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredLight
{
    public class Line3D
    {
        public Point3D[] Points { get; private set; }

        public Line3D(Point3D[] points)
        {
            Points = points;
        }
    }
}
