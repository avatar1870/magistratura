﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace StructuredLight
{
    public class Point3D : IEquatable<Point3D>
    {
        public decimal X { get; private set; }
        public decimal Y { get; private set; }
        public decimal Z { get; private set; }

        public Point3D()
        {
            X = 0M;
            Y = 0M;
            Z = 0M;
        }

        public Point3D(decimal x, decimal y)
        {
            X = x;
            Y = y;
            Z = 0M;
        }

        public Point3D(float x, float y, float z)
        {
            X = (decimal)x;
            Y = (decimal)y;
            Z = (decimal)z;
        }

        public Point3D(decimal x, decimal y, decimal z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Point3D(double x, double y, double z)
        {
            X = Decimal.Parse(x.ToString());
            Y = Decimal.Parse(y.ToString());
            Z = Decimal.Parse(z.ToString());
        }
        public Point3D(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", 
                X.ToString(new CultureInfo("en-US")), 
                Y.ToString(new CultureInfo("en-US")), 
                Z.ToString(new CultureInfo("en-US")));
        }

        public void Write(StreamWriter writer)
        {
            writer.WriteLine(this.ToString());
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(X.ToString(new CultureInfo("en-US")));
            writer.Write(Y.ToString(new CultureInfo("en-US")));
            writer.Write(Z.ToString(new CultureInfo("en-US")));
        }

        public Point3D Multiply(float multiplier)
        {
            return new Point3D(X * (decimal)multiplier, 
                Y * (decimal)multiplier, 
                Z * (decimal)multiplier);
        }

        public Point3D Addition(float offset)
        {
            return new Point3D(X + (decimal)offset,
                Y + (decimal)offset,
                Z + (decimal)offset);
        }

        public Point3D UpdateX(decimal x)
        {
            X = x;

            return this;
        }

        public Point3D UpdateY(decimal y)
        {
            Y = y;

            return this;
        }

        public Point3D UpdateZ(decimal z)
        {
            Z = z;

            return this;
        }

        public bool Equals(Point3D otherPoint3d)
        {
            return otherPoint3d.X == X && otherPoint3d.Y == Y && otherPoint3d.Z == Z;
        }

        public static Point3D operator -(Point3D firstPoint, Point3D secondPoint)
        {
            return new Point3D(firstPoint.X - secondPoint.X,
                firstPoint.Y - secondPoint.Y,
                firstPoint.Z - firstPoint.Z);
        }
        public static Point3D operator +(Point3D firstPoint, Point3D secondPoint)
        {
            return new Point3D(firstPoint.X + secondPoint.X,
                firstPoint.Y + secondPoint.Y,
                firstPoint.Z + firstPoint.Z);
        }

        public static Point3D operator /(Point3D firstPoint, Point3D secondPoint)
        {
            return new Point3D(firstPoint.X / secondPoint.X,
                firstPoint.Y / secondPoint.Y,
                firstPoint.Z / firstPoint.Z);
        }

        public static Point3D Zero()
        {
            return new Point3D(0f, 0f, 0f);
        }

        public static Point3D One()
        {
            return new Point3D(1f, 1f, 1f);
        }

        public static Point3D Read(StreamReader reader)
        {
            const string regex = @"(?<X>[^\s]+)\s+(?<Y>[^\s]+)\s+(?<Z>[^\s]+)";
            const NumberStyles numberStyle = (NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign);

            string data = null;
            float x, y, z;
            Match match = null;

            if (reader == null)
                return null;
            data = reader.ReadLine();

            if (data == null)
                return null;
            match = Regex.Match(data, regex, RegexOptions.IgnoreCase);

            if (!match.Success)
                return null;

            if (!float.TryParse(match.Groups["X"].Value, numberStyle, CultureInfo.InvariantCulture, out x))
                throw new FormatException("Could not parse X coordinate \"{0}\" as a decimal.".Interpolate(match.Groups["X"]));

            if (!float.TryParse(match.Groups["Y"].Value, numberStyle, CultureInfo.InvariantCulture, out y))
                throw new FormatException("Could not parse Y coordinate \"{0}\" as a decimal.".Interpolate(match.Groups["Y"]));

            if (!float.TryParse(match.Groups["Z"].Value, numberStyle, CultureInfo.InvariantCulture, out z))
                throw new FormatException("Could not parse Z coordinate \"{0}\" as a decimal.".Interpolate(match.Groups["Z"]));

            return new Point3D(x, y, z);
        }

        //TODO Fix
        public static Point3D Read(BinaryReader reader)
        {
            const int decimalSize = sizeof(decimal);
            const int vertexSize = (decimalSize * 3);

            if (reader == null)
                return null;

            byte[] data = new byte[vertexSize];
            int bytesRead = reader.Read(data, 0, data.Length);

            if (bytesRead == 0)
                return null;
            //else if (bytesRead != data.Length)
            //  throw new FormatException("Could not convert the binary data to a vertex. Expected {0} bytes but found {1}.".Interpolate(vertexSize, bytesRead));

            System.Windows.Forms.MessageBox.Show(BitConverter.ToSingle(data, 0).ToString());

            return new Point3D(x: BitConverter.ToSingle(data, 0),
                y: BitConverter.ToSingle(data, decimalSize),
                z: BitConverter.ToSingle(data, (decimalSize * 2)));
        }

        public static Point3D GetRandom(int multiplier = 1)
        {
            Random rnd = new Random(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), NumberStyles.HexNumber));

            return new Point3D(
                rnd.NextDouble() * multiplier,
                rnd.NextDouble() * multiplier,
                rnd.NextDouble() * multiplier);
        }
    }
}