﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace StructuredLight
{
    public partial class Form3 : Form
    {
        private STLModelEditor _stlModelEditor;

        private List<Point3D> _pointCloud;
        private List<Triangle> _triangles;
        private List<Line3D> _lines;
        private Tetrahedron _firstTetra;

        private Point3D _zoomPoint;

        private float _rotationOffset;

        public Form3(Delaunay delone)
        {
            InitializeComponent();

            this.MouseWheel += Form3_MouseWheel;

            _stlModelEditor = new STLModelEditor(openGLControl1.OpenGL);

            _zoomPoint = new Point3D(-1f, -1f, -8f);

            _rotationOffset = 0f;

            _triangles = delone.OutsideTriangleList;
            _lines = delone.FirstTetra.GetOuterLines().ToList();
            _firstTetra = delone.FirstTetra;
            _pointCloud = delone.PointCloud;
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MessageBox.Show("Вы нажали Левую кнопку, координаты курсора - " + e.Location);
            }
        }

        private void Form3_MouseMove(object sender, MouseEventArgs e)
        {
            MessageBox.Show(e.Delta.ToString());
        }

        private void Form3_MouseWheel(object sender, MouseEventArgs e)
        {
            _zoomPoint.UpdateZ(_zoomPoint.Z + e.Delta / 5000M);
        }

        private void openGLControl1_OpenGLDraw(object sender, SharpGL.RenderEventArgs args)
        {
            _stlModelEditor.SetPointOfView(_zoomPoint, _firstTetra.CenterOuterCircle.Vertex, _rotationOffset);

            _stlModelEditor.RenderTetrahedrones(new List<Tetrahedron>() { _firstTetra }.ToArray());
            _stlModelEditor.RenderTriangles(_triangles.ToArray());
            _stlModelEditor.RenderPoints(_pointCloud.ToArray(), Color.Green, 10f);
            _stlModelEditor.RenderLines(_lines, Color.White);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            _zoomPoint.UpdateX(_zoomPoint.X + (e.X - _mousePosition.X) / 1000M);
            _zoomPoint.UpdateY(_zoomPoint.Y - (e.Y - _mousePosition.Y) / 1000M);
        }

        private Point3D _mousePosition;

        private void openGLControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _mousePosition = new Point3D(e.Location.X, e.Location.Y);

                openGLControl1.MouseMove += OnMouseMove;                               
            }
        }

        private void openGLControl1_MouseUp(object sender, MouseEventArgs e)
        {
            openGLControl1.MouseMove -= OnMouseMove;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _zoomPoint = new Point3D(float.Parse(textBox1.Text), float.Parse(textBox2.Text), float.Parse(textBox3.Text));
        }

        private void openGLControl1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Left)
            {
                if (ModifierKeys == Keys.Control)
                {
                    _rotationOffset += (e.KeyCode == Keys.Right) ? 1 : -1;
                }
            }
        }
    }
}
