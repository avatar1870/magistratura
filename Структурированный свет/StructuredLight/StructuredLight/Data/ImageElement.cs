﻿using System;
using System.Collections.Generic;

namespace StructuredLight.Data
{
    class ImageElementLine
    {
        public ImageElementLine()
        {
            LineNumberString = String.Empty;
        }

        public int LineNumber
        {
            get
            {
                return Convert.ToInt32(LineNumberString, 2);
            }
        }
        public string LineNumberString { get; private set; }

        public void AddToLineNumberString(string s)
        {
            LineNumberString += s;
        }        
    }

    public class ImageRepresentation
    {
        private Dictionary<Vector2Int, ImageElementLine> pixelLineNumbers;

        public ImageRepresentation()
        {
            pixelLineNumbers = new Dictionary<Vector2Int, ImageElementLine>();
        }

        public int this[int x, int y]
        {
            get
            {
                Vector2Int point = new Vector2Int(x, y);

                if (pixelLineNumbers.ContainsKey(point))
                {
                    return pixelLineNumbers[point].LineNumber;
                }

                return 0;
            }
        }

        public void AddOrUpdatePoint(int x, int y, string lineCode)
        {
            Vector2Int point = new Vector2Int(x, y);

            if (!pixelLineNumbers.ContainsKey(point))
            {
                pixelLineNumbers.Add(point, new ImageElementLine());
            }

            pixelLineNumbers[point].AddToLineNumberString(lineCode);
        }
    }
}
