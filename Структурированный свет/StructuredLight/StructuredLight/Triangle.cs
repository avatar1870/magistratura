﻿using System;
using System.Collections.Generic;

namespace StructuredLight
{
    public class Triangle : IEquatable<Triangle>
    {
        public Vector3D Vertex1 { get; private set; }
        public Vector3D Vertex2 { get; private set; }
        public Vector3D Vertex3 { get; private set; }

        public Vector3D[] Vertices { get; private set; }

        public Triangle(Vector3D vertex1, Vector3D vertex2, Vector3D vertex3)
        {
            Vertex1 = vertex1;
            Vertex2 = vertex2;
            Vertex3 = vertex3;

            Vertices = new Vector3D[3] 
            {
                vertex1, vertex2, vertex3
            };
        }

        public Vector3D GetNormal()
        {
            Vector3D edge1 = new Vector3D(new Point3D(Vertex2.Vertex.X - Vertex1.Vertex.X, 
                Vertex2.Vertex.Y - Vertex1.Vertex.Y, 
                Vertex2.Vertex.Z - Vertex1.Vertex.Z));
            Vector3D edge2 = new Vector3D(new Point3D(Vertex3.Vertex.X - Vertex1.Vertex.X, 
                Vertex3.Vertex.Y - Vertex1.Vertex.Y, 
                Vertex3.Vertex.Z - Vertex1.Vertex.Z));

            Vector3D normal = edge1.Cross(edge2);

            return normal.GetNormalization();
        }

        public void TurnBack()
        {
            Vector3D tmp = Vertex3;
            Vertex3 = Vertex1;
            Vertex1 = tmp;

            Vertices[0] = Vertex1;
            Vertices[2] = Vertex3;
        }

        /*public decimal GetArea()
        {
            Vector3D AB = Vertex2.Sub(Vertex1);
            Vector3D AC = Vertex3.Sub(Vertex1);
            Vector3D cross = AB.Cross(AC);

            decimal result = Math.Sqrt((double)(cross.Vertex.X * cross.Vertex.X + 
                cross.Vertex.Y * cross.Vertex.Y + 
                cross.Vertex.Z * cross.Vertex.Z)) / 2.0M;

            return result;
        }*/

        public Facet GetFacet()
        {
            List<Vertex> v = new List<Vertex>();

            v.Add(new Vertex(Vertex1.Vertex.X, Vertex1.Vertex.Y, Vertex1.Vertex.Z));
            v.Add(new Vertex(Vertex2.Vertex.X, Vertex2.Vertex.Y, Vertex2.Vertex.Z));
            v.Add(new Vertex(Vertex3.Vertex.X, Vertex3.Vertex.Y, Vertex3.Vertex.Z));

            return new Facet(v, 0);
        }

        public override string ToString()
        {
            return string.Format("<{0}-{1}-{2}>", Vertex1, Vertex2, Vertex3);
        }

        public bool Equals(Triangle src)
        {
            foreach (Vector3D my in Vertices)
            {
                bool match = false;
                foreach (Vector3D t in src.Vertices)
                {
                    if (my.Equals(t)) match = true;
                }
                if (!match) return false;
            }
            return true;
        }
    }
}
