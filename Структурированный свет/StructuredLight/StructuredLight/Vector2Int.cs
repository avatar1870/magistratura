﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredLight
{
    public struct Vector2Int
    {
        public int x { get; }
        public int y { get; }

        public Vector2Int(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
