﻿using SharpGL;
using System.Collections.Generic;
using System.Drawing;

namespace StructuredLight
{
    class STLModelEditor
    {
        private OpenGL _gl;

        public STLModelEditor(OpenGL gl)
        {
            _gl = gl;
        }

        public void RenderTetrahedrones(Tetrahedron[] tetrahedrones)
        {
            _gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            if (tetrahedrones != null)
            {
                foreach (Tetrahedron tetrahedron in tetrahedrones)
                {
                    RenderPoints(tetrahedron.Vertices.ToPointsList().ToArray(), Color.Aqua, 7f);
                }
            }
        }

        public void RenderTriangles(Triangle[] triangles)
        {
            _gl.Begin(OpenGL.GL_TRIANGLES);

            if (triangles != null)
            {
                foreach (Triangle triangle in triangles)
                {
                    _gl.Color(1.0f, 0.0f, 0.0f);
                    _gl.Vertex((double)triangle.Vertex1.Vertex.X, (double)triangle.Vertex1.Vertex.Y, (double)triangle.Vertex1.Vertex.Z);

                    _gl.Color(0.0f, 1.0f, 0.0f);
                    _gl.Vertex((double)triangle.Vertex2.Vertex.X, (double)triangle.Vertex2.Vertex.Y, (double)triangle.Vertex2.Vertex.Z);

                    _gl.Color(0.0f, 0.0f, 1.0f);
                    _gl.Vertex((double)triangle.Vertex3.Vertex.X, (double)triangle.Vertex3.Vertex.Y, (double)triangle.Vertex3.Vertex.Z);
                }
            }

            _gl.End();

            _gl.Flush();
        }

        public void RenderPoints(Point3D[] points, Color color, float size = 1f)
        {
            _gl.PointSize(size);
            _gl.Begin(SharpGL.Enumerations.BeginMode.Points);           

            foreach (var point in points)
            {
                _gl.Color(color.R.ToRBG1(), color.G.ToRBG1(), color.B.ToRBG1());
                _gl.Vertex((double)point.X, (double)point.Y, (double)point.Z);
            }

            _gl.End();
        }

        public void RenderLines(List<Line3D> lines, Color color)
        {
            _gl.Begin(SharpGL.Enumerations.BeginMode.Lines);

            _gl.Color(color.R.ToRBG1(), color.G.ToRBG1(), color.B.ToRBG1());

            foreach(Line3D line in lines)
            {
                foreach (Point3D point3D in line.Points)
                {
                    _gl.Vertex((double)point3D.X, (double)point3D.Y, (double)point3D.Z);
                }
            }

            _gl.End();
        }

        public void SetPointOfView(Point3D point, Point3D rotate, float rotatePoint = 0)
        {
            _gl.LoadIdentity();

            _gl.Translate((double)point.X, (double)point.Y, (double)point.Z);

            _gl.Rotate(rotatePoint, (double)rotate.X, (double)rotate.Y, (double)rotate.Z);
        }
    }
}
