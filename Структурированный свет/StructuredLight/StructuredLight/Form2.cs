﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace StructuredLight
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void chart1_Click(object sender, System.EventArgs e)
        {

        }

        public void ShowShow(List<Vector2Int> points)
        {
            Vizualizer vizualizer = new Vizualizer(chart1);
            vizualizer.Draw2D(points);
        }

        private void Form2_Load(object sender, System.EventArgs e)
        {

        }
    }
}
