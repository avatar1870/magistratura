﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace StructuredLight
{
    public class Delaunay
    {
        /// <summary>
        /// Облако точек
        /// </summary>
        public List<Point3D> PointCloud { private set; get; }

        /// <summary>
        /// Тетраэдр, включающий все точки
        /// </summary>
        public Tetrahedron FirstTetra { private set; get; }
        /// <summary>
        /// Коллекция тетраэдров
        /// </summary>
        public List<Tetrahedron> TetraList { private set; get; }
        /// <summary>
        /// Коллекция треугольников, полученных из коллекции тетраэдров
        /// </summary>
        public List<Triangle> AllTriangleList { private set; get; }
        /// <summary>
        /// Коллекция внешних треугольников, полученных из коллекции тетраэдров
        /// </summary>
        public List<Triangle> OutsideTriangleList { private set; get; }

        public Delaunay(List<Point3D> pointsList)
        {
            PointCloud = pointsList;

            FirstTetra = GetFirstTetra(pointsList);

            TetraList = GetTetraList(pointsList.ToVectorsList());

            AllTriangleList = TetraToTriangle(TetraList);

            OutsideTriangleList = FilterOutSidePoint(AllTriangleList);
        }

        /// <summary>
        /// <para>Удаляет тот же трегольник и внутренние треугольники</para>
        /// </summary>
        /// <returns>Список внешних треугольников</returns>
        private List<Triangle> FilterOutSidePoint(List<Triangle> allTriangleList)
        {
            List<Triangle> dstList = new List<Triangle>();
            bool[] isSameTriangle = new bool[allTriangleList.Count];
            for (int i = 0; i < allTriangleList.Count - 1; i++)
            {
                for (int j = i + 1; j < allTriangleList.Count; j++)
                {
                    if (allTriangleList[i].Equals(allTriangleList[j]))
                    {
                        isSameTriangle[i] = isSameTriangle[j] = true;
                    }
                }
            }

            for (int i = 0; i < isSameTriangle.Length; i++)
            {
                if (!isSameTriangle[i])
                {
                    dstList.Add(allTriangleList[i]);
                }
            }
            return dstList;
        }

        /// <summary>
        /// Возвращает внешние треугольники из списка тетраэдров
        /// </summary>
        /// <returns>Список внешних треугольников</returns>
        private List<Triangle> TetraToTriangle(List<Tetrahedron> tetraList)
        {
            List<Triangle> triList = new List<Triangle>();

            foreach (var tetra in tetraList)
            {
                Vector3D v1 = tetra.Vertex1;
                Vector3D v2 = tetra.Vertex2;
                Vector3D v3 = tetra.Vertex3;
                Vector3D v4 = tetra.Vertex4;

                Triangle tri1 = new Triangle(v1, v2, v3);
                Triangle tri2 = new Triangle(v1, v3, v4);
                Triangle tri3 = new Triangle(v1, v4, v2);
                Triangle tri4 = new Triangle(v4, v3, v2);

                Vector3D normal;
                normal = tri1.GetNormal();

                if (normal.Dot(v1) > normal.Dot(v4))
                {
                    tri1.TurnBack();
                }

                normal = tri2.GetNormal();
                if (normal.Dot(v1) > normal.Dot(v2))
                {
                    tri2.TurnBack();
                }

                normal = tri3.GetNormal();
                if (normal.Dot(v1) > normal.Dot(v3))
                {
                    tri3.TurnBack();
                }

                normal = tri4.GetNormal();
                if (normal.Dot(v2) > normal.Dot(v1))
                {
                    tri4.TurnBack();
                }

                triList.Add(tri1);
                triList.Add(tri2);
                triList.Add(tri3);
                triList.Add(tri4);
            }

            return triList;
        }

        /// <summary>
        /// Вычисляет триангуляцию Делоне
        /// </summary>
        /// <param name="vectorsList">AllPoints</param>
        /// <returns>Коллекция тетраэдров</returns>
		private List<Tetrahedron> GetTetraList(List<Vector3D> vectorsList)
        {
            List<Tetrahedron> tetraList = new List<Tetrahedron>();
            tetraList.Add(this.FirstTetra);

            List<Tetrahedron> tmpTList = new List<Tetrahedron>();
            List<Tetrahedron> newTList = new List<Tetrahedron>();
            List<Tetrahedron> removeTList = new List<Tetrahedron>();

            foreach (var point in vectorsList)
            {
                tmpTList.Clear();
                newTList.Clear();
                removeTList.Clear();

                foreach (Tetrahedron t in tetraList)
                {
                    if (t.CenterOuterCircle != null && t.RadiusOuterCircle > Common.GetDist(point, t.CenterOuterCircle))
                    {
                        tmpTList.Add(t);
                    }
                }

                foreach (var t1 in tmpTList)
                {
                    tetraList.Remove(t1);

                    Vector3D v1 = t1.Vertex1;
                    Vector3D v2 = t1.Vertex2;
                    Vector3D v3 = t1.Vertex3;
                    Vector3D v4 = t1.Vertex4;

                    newTList.Add(new Tetrahedron(v1, v2, v3, point));
                    newTList.Add(new Tetrahedron(v1, v2, v4, point));
                    newTList.Add(new Tetrahedron(v1, v3, v4, point));
                    newTList.Add(new Tetrahedron(v2, v3, v4, point));
                }


                bool[] isRedundancy = new bool[newTList.Count];
                for (int i = 0; i < newTList.Count - 1; i++)
                {
                    for (int j = i + 1; j < newTList.Count; j++)
                    {
                        if (newTList[i].Equal(newTList[j]))
                        {
                            isRedundancy[i] = isRedundancy[j] = true;
                        }
                    }
                }

                for (int i = 0; i < newTList.Count; i++)
                {
                    if (!isRedundancy[i])
                    {
                        tetraList.Add(newTList[i]);
                    }
                }
            }

            Vector3D[] outer = new Vector3D[] 
            {
                this.FirstTetra.Vertex1,
                this.FirstTetra.Vertex2,
                this.FirstTetra.Vertex3,
                this.FirstTetra.Vertex4
            };

            bool isOuter = false;
            var count = 0;
            for (int i = tetraList.Count - 1; i >= 0; i--)
            {
                isOuter = false;
                foreach (var t4Point in tetraList[i].Vertices)
                {
                    foreach (var outerPoint in outer)
                    {
                        if (t4Point.Vertex.X == outerPoint.Vertex.X &&
                            t4Point.Vertex.Y == outerPoint.Vertex.Y &&
                            t4Point.Vertex.Z == outerPoint.Vertex.Z)
                        {
                            isOuter = true;
                        }
                    }
                }
                if (isOuter)
                {
                    tetraList.RemoveAt(i);
                    count++;
                    //Console.WriteLine(count);
                }
            }

            return tetraList;
        }

        /// <summary>
        /// Вычисляет тетраэд, содержащий все точки
        /// </summary>
        /// <returns>Первый тетраэдр</returns>
        private static Tetrahedron GetFirstTetra(List<Point3D> pointsList)
        {
            decimal xMin = pointsList.Min(value => value.X);
            decimal yMin = pointsList.Min(value => value.Y);
            decimal zMin = pointsList.Min(value => value.Z);

            decimal xMax = pointsList.Max(value => value.X);
            decimal yMax = pointsList.Max(value => value.Y);
            decimal zMax = pointsList.Max(value => value.Z);

            //Размер прямоугольника
            decimal width = xMax - xMin;
            decimal height = yMax - yMin;
            decimal depth = zMax - zMin;

            //Центр шара
            decimal cX = width / 2 + xMin;
            decimal cY = height / 2 + yMin;
            decimal cZ = depth / 2 + zMin;
            Vector3D center = new Vector3D(new Point3D(cX, cY, cZ));

            //Радиус
            //0.1f в дополнение
            decimal radius = Common.GetDist(
                new Vector3D(new Point3D(xMax, yMax, zMax)),
                new Vector3D(new Point3D(xMin, yMin, zMin))) / 2 + 0.1M;

            Vector3D p1 = new Vector3D(new Point3D(center.Vertex.X, center.Vertex.Y + 3.0M*radius, center.Vertex.Z));
            Vector3D p2 = new Vector3D(new Point3D(center.Vertex.X + 2 * (decimal)Math.Sqrt(2) * radius, 
                center.Vertex.Y - radius,
                center.Vertex.Z));
            Vector3D p3 = new Vector3D(new Point3D(-(decimal)Math.Sqrt(2) * radius + center.Vertex.X,
                -radius + center.Vertex.Y, 
                (decimal)Math.Sqrt(6) * radius + center.Vertex.Z));
            Vector3D p4 = new Vector3D(new Point3D(-(decimal)Math.Sqrt(2) * radius + center.Vertex.X, 
                -radius + center.Vertex.Y, 
                -(decimal)Math.Sqrt(6) * radius + center.Vertex.Z));

            return new Tetrahedron(p1, p2, p3, p4);
        }
    }
}
