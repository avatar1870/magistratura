﻿using System;
using System.Linq;

namespace StructuredLight
{
    public class Tetrahedron : IEquatable<Tetrahedron>
    {
        public Vector3D Vertex1 { get; private set; }
        public Vector3D Vertex2 { get; private set; }
        public Vector3D Vertex3 { get; private set; }
        public Vector3D Vertex4 { get; private set; }

        public Vector3D[] Vertices { get; private set; }

        public Vector3D CenterOuterCircle { get; private set; }

        public Decimal RadiusOuterCircle { get; private set; } 

        public Tetrahedron(Vector3D vertex1, Vector3D vertex2, Vector3D vertex3, Vector3D vertex4)
        {
            Vertex1 = vertex1;
            Vertex2 = vertex2;
            Vertex3 = vertex3;
            Vertex4 = vertex4;

            Vertices = new Vector3D[4]
            {
                vertex1,
                vertex2,
                vertex3,
                vertex4
            };

            GetCircleCenter();
        }

        public Line3D[] GetOuterLines()
        {
            return new Line3D[] {
                new Line3D(new Point3D[] { Vertex1.Vertex, Vertex2.Vertex }),
                new Line3D(new Point3D[] { Vertex1.Vertex, Vertex3.Vertex }),
                new Line3D(new Point3D[] { Vertex1.Vertex, Vertex4.Vertex }),
                new Line3D(new Point3D[] { Vertex2.Vertex, Vertex3.Vertex }),
                new Line3D(new Point3D[] { Vertex2.Vertex, Vertex4.Vertex }),
                new Line3D(new Point3D[] { Vertex3.Vertex, Vertex4.Vertex }),                
            };
        }

        private void GetCircleCenter()
        {
            decimal[,] A = new decimal[,] {
            {
                Vertex2.Vertex.X - Vertex1.Vertex.X,
                Vertex2.Vertex.Y - Vertex1.Vertex.Y,
                Vertex2.Vertex.Z - Vertex1.Vertex.Z
            },
            {
                Vertex3.Vertex.X - Vertex1.Vertex.X,
                Vertex3.Vertex.Y - Vertex1.Vertex.Y,
                Vertex3.Vertex.Z - Vertex1.Vertex.Z
            },
            {
                Vertex4.Vertex.X - Vertex1.Vertex.X,
                Vertex4.Vertex.Y - Vertex1.Vertex.Y,
                Vertex4.Vertex.Z - Vertex1.Vertex.Z
            }
            };

            decimal[] b = new decimal[]{
            0.5M * (
                Vertex2.Vertex.X * Vertex2.Vertex.X - 
                Vertex1.Vertex.X * Vertex1.Vertex.X +
                Vertex2.Vertex.Y * Vertex2.Vertex.Y -
                Vertex1.Vertex.Y * Vertex1.Vertex.Y +
                Vertex2.Vertex.Z * Vertex2.Vertex.Z -
                Vertex1.Vertex.Z * Vertex1.Vertex.Z),
            0.5M * (
                Vertex3.Vertex.X * Vertex3.Vertex.X -
                Vertex1.Vertex.X * Vertex1.Vertex.X +
                Vertex3.Vertex.Y * Vertex3.Vertex.Y -
                Vertex1.Vertex.Y * Vertex1.Vertex.Y +
                Vertex3.Vertex.Z * Vertex3.Vertex.Z -
                Vertex1.Vertex.Z * Vertex1.Vertex.Z),
            0.5M * (
                Vertex4.Vertex.X * Vertex4.Vertex.X -
                Vertex1.Vertex.X * Vertex1.Vertex.X +
                Vertex4.Vertex.Y * Vertex4.Vertex.Y -
                Vertex1.Vertex.Y * Vertex1.Vertex.Y +
                Vertex4.Vertex.Z * Vertex4.Vertex.Z -
                Vertex1.Vertex.Z * Vertex1.Vertex.Z)
            };

            decimal[] x = new decimal[3];

            if (Gauss(A, b, x) == 0)
            {
                CenterOuterCircle = null;
                RadiusOuterCircle = -1;
            }
            else
            {
                CenterOuterCircle = new Vector3D(new Point3D(x[0], x[1], x[2]));
                RadiusOuterCircle = Common.GetDist(CenterOuterCircle, Vertex1);
            }
        }

        private decimal Gauss(decimal[,] a, decimal[] b, decimal[] x)
        {
            int n = a.GetLength(0);
            int[] ip = new int[n];
            decimal det = Lu(a, ip);

            if (det != 0)
            {
                Solve(a, b, ip, x);
            }

            return det;
        }

        private decimal Lu(decimal[,] a, int[] ip)
        {
            int n = a.GetLength(0);
            decimal[] weight = new decimal[n];

            for (int k = 0; k < n; k++)
            {
                ip[k] = k;
                decimal u = 0;

                 for (int j = 0; j < n; j++)
                {
                    decimal t = Math.Abs(a[k, j]);
                    if (t > u) u = t;
                }
                if (u == 0) return 0;
                weight[k] = 1 / u;
            }


            decimal det = 1;
            for (int k = 0; k < n; k++)
            {
                decimal u = -1;
                int m = 0;

                for (int i = k; i < n; i++)
                {
                    int ii = ip[i];
                    decimal t = Math.Abs(a[ii, k]) * weight[ii];
                    if (t > u) { u = t; m = i; }
                }

                int ik = ip[m];

                if (m != k)
                {
                    ip[m] = ip[k]; ip[k] = ik;
                    det = -det;
                }
                u = a[ik, k]; det *= u;
                if (u == 0) return 0;

                for (int i = k + 1; i < n; i++)
                {
                    int ii = ip[i];
                    decimal t = (a[ii, k] /= u);
                    for (int j = k + 1; j < n; j++)
                    {
                        a[ii, j] -= t * a[ik, j];
                    }
                }
            }
            return det;
        }

        private void Solve(decimal[,] a, decimal[] b, int[] ip, decimal[] x)
        {
            int n = a.GetLength(0);

            for (int i = 0; i < n; i++)
            {
                int ii = ip[i]; decimal t = b[ii];
                for (int j = 0; j < i; j++) t -= a[ii, j] * x[j];
                x[i] = t;
            }

            for (int i = n - 1; i >= 0; i--)
            {
                decimal t = x[i]; int ii = ip[i];
                for (int j = i + 1; j < n; j++) t -= a[ii, j] * x[j];
                x[i] = t / a[ii, i];
            }
        }

        public bool Equal(Tetrahedron other)
        {
            var hitCount = 0;

            for (int i = 0; i < Vertices.Count(); i++)
            {
                for (int j = 0; j < other.Vertices.Count(); j++)
                {
                    if (Vertices[i].Equals(other.Vertices[j]))
                    {
                        hitCount++;
                    }
                }
            }

            return hitCount == 4;
        }

        public override string ToString()
        {
            return string.Format("==[{0}-{1}-{2}-{3}]==", 
                Vertex1.ToString(), Vertex2.ToString(), Vertex3.ToString(), Vertex4.ToString());
        }

        public bool Equals(Tetrahedron other)
        {
            return Equal(other);
        }
    }
}
