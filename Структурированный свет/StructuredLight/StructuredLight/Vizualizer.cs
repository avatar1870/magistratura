﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

public class Vizualizer
{
    private Chart _chart;

    public Vizualizer(Chart chart)
	{
        _chart = chart;
	}

    public void Draw2D(List<StructuredLight.Vector2Int> points)
    {
        _chart.ChartAreas.Add(new ChartArea("Math functions"));

        Series mySeriesOfPoint = new Series("Sinus");
        mySeriesOfPoint.ChartType = SeriesChartType.Point;

        foreach (StructuredLight.Vector2Int point in points)
        {
            mySeriesOfPoint.Points.AddXY(point.x, point.y);
        }

        _chart.Series.Add(mySeriesOfPoint);
    }
}
