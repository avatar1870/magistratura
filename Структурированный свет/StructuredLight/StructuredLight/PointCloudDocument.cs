﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace StructuredLight
{
    class PointCloudDocument
    {
        private const int DefaultBufferSize = 1024;
        private const string DocumentHeader = "Point Cloud";
        private const string Extension = "pointcloud";

        public string Name { get; private set; }
        public IList<Point3D> PointCloud { get; private set; }

        public PointCloudDocument()
        {
            Name = Guid.NewGuid().ToString();
        }

        public PointCloudDocument(IEnumerable<Point3D> pointCloud) : this ()
        {            
            PointCloud = pointCloud.ToList();
        }

        public PointCloudDocument(string name, IEnumerable<Point3D> pointCloud)
        {
            Name = name;
            PointCloud = pointCloud.ToList();
        }

        public void AddPoint(Point3D point)
        {
            if (PointCloud == null)
            {
                PointCloud = new List<Point3D>();
            }

            PointCloud.Add(point);
        }

        public void SaveAsText(string path)
        {
            if (path.IsNullOrEmpty())
                throw new ArgumentNullException("path");

            Directory.CreateDirectory(Path.GetDirectoryName(path));

            using (Stream stream = File.Create(Path.Combine(path, string.Format("{0}.{1}", Name, Extension))))
                WriteText(stream);
        }

        public void SaveAsBinary(string path)
        {
            if (path.IsNullOrEmpty())
                throw new ArgumentNullException("path");

            Directory.CreateDirectory(Path.GetDirectoryName(path));

            using (Stream stream = File.Create(Path.Combine(path, string.Format("{0}.{1}", Name, Extension))))
                WriteBinary(stream);
        }

        public void WriteText(Stream stream)
        {
            using (StreamWriter writer = new StreamWriter(stream, Encoding.ASCII, DefaultBufferSize, true))
            {
                writer.WriteLine(DocumentHeader);

                PointCloud.ForEach(item => item.Write(writer));
            }
        }

        public static PointCloudDocument Open(string path)
        {
            if (path.IsNullOrEmpty())
                throw new ArgumentNullException("path");

            using (StreamReader stream = File.OpenText(path))
                return Read(stream);
        }

        public void WriteBinary(Stream stream)
        {
            using (BinaryWriter writer = new BinaryWriter(stream, Encoding.ASCII, true))
            {
                byte[] header = Encoding.ASCII.GetBytes(DocumentHeader);
                byte[] headerFull = new byte[80];

                Buffer.BlockCopy(header, 0, headerFull, 0, Math.Min(header.Length, headerFull.Length));

                writer.Write(headerFull);

                PointCloud.ForEach(item => item.Write(writer));
            }
        }

        public static PointCloudDocument Read(StreamReader reader)
        {
            if (reader == null)
                return null;

            string header = reader.ReadLine();

            PointCloudDocument document = new PointCloudDocument();
            Point3D currentPoint = null;

            while ((currentPoint = Point3D.Read(reader)) != null)
            {
                document.AddPoint(currentPoint);
            }

            return document;
        }

        public static PointCloudDocument Read(BinaryReader reader)
        {
            if (reader == null)
                return null;

            byte[] buffer = new byte[80];
            PointCloudDocument document = new PointCloudDocument();
            Point3D currentPoint = null;

            buffer = reader.ReadBytes(80);

            while ((reader.BaseStream.Position != reader.BaseStream.Length)
                && (currentPoint = Point3D.Read(reader)) != null)
            {
                document.AddPoint(currentPoint);
            }

            return document;
        }
    }
}
