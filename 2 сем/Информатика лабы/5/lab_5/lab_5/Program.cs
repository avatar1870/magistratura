﻿using System;

namespace lab_5
{
    class Program
    {
        private const double startX = 0.2;
        private const double endX = 2.4;

        private const double step = 0.1;

        static void Main(string[] args)
        {
            double currentX = 0;

            int a = 0;
            int b = 0;

            Console.WriteLine("A:");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("B:");
            b = int.Parse(Console.ReadLine());

            int count = (int)((endX - startX) / step) + 1;
            currentX = startX;
            for (int i = 0; i <= count; i++)
            {
                Console.WriteLine("X: " + currentX + " Y: " + CalculateY(a, b, currentX) + Environment.NewLine);

                currentX += step;
            }

            Console.ReadLine();
        }

        private static double CalculateY(double a, double b, double currentX)
        {
            return a * Math.Pow(currentX, 2) + b * currentX * Math.Tan(Math.Pow(currentX, 2) - 0.15) - Math.Exp(currentX / 2);
        }
    }
}
